class UsersController < ApplicationController
  def index
  	@users = User.all
  end

  def create
  	 @user = User.new(user_params)

    if @user.save
      upload_picture
      redirect_to [:admin, @user]
    else
      render 'new'
    end
  end

  def new
    @user = User.new
  end

	def edit
  	@user = User.find(params[:id])
	end

  def update
    @user = User.find(params[:id])

    if @user.update(user_params)
      upload_picture
      redirect_to [:admin, @user]
    else
      render 'edit'
    end
  end

  def destroy
    @user = User.destroy(params[:id])
    redirect_to user_url
  end

  private

  def user_params
    params.require(:user).permit(:name)
  end
end
